﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mirrorPositionOnInput : MonoBehaviour {
    Vector2 pos;
    ActorPlayer ap;
	// Use this for initialization
	void Awake () {
        ap = GetComponentInParent<ActorPlayer>();
        pos = transform.localPosition;
        pos.x = Mathf.Abs(pos.x);
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        transform.position = (Vector2) transform.parent.position + new Vector2(pos.x * ap.input.facingDirection, pos.y);
    }
}
