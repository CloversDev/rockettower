﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

    [SerializeField] private string mainMenu;
    [SerializeField] private GameObject pauseMenuCanvas;
    private bool isPaused = false;

    void Update()
    {
       

        if (Input.GetKeyDown(KeyCode.P))
        {
            isPaused = !isPaused;
            if (isPaused)
            {
                pauseMenuCanvas.SetActive(true);
                Time.timeScale = 0.0f;
            }
            else
            {
                pauseMenuCanvas.SetActive(false);
                Time.timeScale = 1.0f;
            }
            
        }
            
    }

    public void Resume()
    {
        isPaused = false;
        pauseMenuCanvas.SetActive(false);
        Time.timeScale = 1.0f;
    }

    public void QuitToMainMenu()
    {
        SceneManager.LoadScene(mainMenu);
    }

}
