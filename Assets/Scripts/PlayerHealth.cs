﻿using UnityEngine;

public class PlayerHealth : MonoBehaviour {

    [SerializeField] private float health;
    [SerializeField] private int lives;

    public Vector2 OnDamagePush = new Vector2(5, 5);
    CharacterController2D cc;
    private float maxHealth;

    void Awake()
    {
        maxHealth = health;
        cc = GetComponent<CharacterController2D>();
    }

    public int CurrentLives
    {
        get { return lives; }
    }

    public float Quantity
    {
        get { return health; }
    }
    public virtual void Kill()
    {
        health = 0;
        die();
    }
    private void die()
    {
        if (lives <= 0)
        {
            LevelManager.instance.deaths++;
            Destroy(gameObject);
        }
        else
        {
            lives--;
            health = maxHealth;
            RespawnPointManager.RequestRespawn(gameObject);           
        }
    }
    public virtual void Hurt(float damage, int dir)
    {
        health -= damage;
        if (health <= 0)
        {
            die();
        }
        Debug.Log("Jumping to:" + dir * OnDamagePush.x + "," + OnDamagePush.y);
        cc.ignoreFloor = true;
        cc.Push(dir * OnDamagePush.x, OnDamagePush.y);
    }
}
