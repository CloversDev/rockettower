﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    [SerializeField] private string winScene;
    [SerializeField] private int cantPlayers;

    void Awake()
    {
        cantPlayers -= 1;
    }
    public void Win()
    {
        SceneManager.LoadScene(winScene);
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene("FinalTestScene");
        }
    }

}
