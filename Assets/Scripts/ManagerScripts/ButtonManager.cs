﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour {

    [SerializeField] private GameObject panel;

    public void PressPlayButton(string level)
    {
        SceneManager.LoadScene(level);
    }

    public void PressExit()
    {
        Application.Quit();
    }

    public void PressSettings()
    {
        panel.SetActive(true);
    }

}
