﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDamageEnemy : MonoBehaviour {
    int dir = 1;
    [SerializeField] private float damage;
    private InputListener input;
    private ActorPlayer actorPlayer;
    public float cooldown = 0.2f;
    float Countdown;
    public SpriteRenderer r;
    void Start()
    {
        input = GetComponentInParent<InputListener>();
        r = GetComponent<SpriteRenderer>();
        actorPlayer = GetComponentInParent<ActorPlayer>();
        Countdown = cooldown;
        
    }
    private void Update()
    {

        if (Countdown > 0)
        {
            r.color = new Color(1, 1, 1, 1);
            Countdown -= Time.deltaTime;
        }
        else
        {
            r.color = new Color(1, 1, 1, 0);
        }       
    }
    void OnTriggerStay2D(Collider2D other)
    {
        if (input.Fire1ButtonPress())
        {
            Countdown = cooldown;
            Debug.Log("Button1");
            switch (other.tag)
            {
                case "Player":
                    PlayerHealth playerHealth = other.GetComponent<PlayerHealth>();
                    Debug.Log("Hurt Player" + actorPlayer.input.facingDirection);
                    playerHealth.Hurt(0, actorPlayer.input.facingDirection);
                    break;
                case "Enemy":
                    EnemyHealth enemyHealth = other.GetComponent<EnemyHealth>();                    
                    if (enemyHealth != null)
                    {
                        Debug.Log("Hurt Enemy" + actorPlayer.input.facingDirection);
                        enemyHealth.Hurt(damage, actorPlayer.input.facingDirection);
                    }
                    break;
            }
        }       
    }

}
